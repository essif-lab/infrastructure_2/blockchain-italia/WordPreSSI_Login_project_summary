# WordPreSSI Login project summary

WordPreSSI is an authentication plugin enabling SSI/eSSI Verifiable Credentials for logging into WordPress sites, which currently power 39% of websites at global level.

## Table of contents
* [Overview and goals](#overview-and-goals)
* [Innovation and Technology Description](#innovation-and-technology-description)
* [Architecture](#architecture)
* [About our team](#about-our-team)

## Overview and goals

The WordPreSSI plugin:
- gives back to users the control over their own data, ensuring a privacy-by design, decentralized and passwordless login system; it enables simpler and more secure Internet transactions: users will login minimizing the sharing of personal data and without relying on centralized parties for storage and management.
- significantly increases security for all WordPress sites. With the current system, all the WordPress sites have repository of usernames and password, which are subject to serious risk of hacking. With our plugin, passwords will no longer be stored on the servers as credentials are passed at the start of user sessions via VC.
- gives the community of open source developers the opportunity to extend the plugin with other functionalities.

## Innovation and Technology Description

The SSI Plugin can be configured in the admin area. It will be possible to configure the issuing of credentials for users registering in the site and it will be also possible to configure verification policies in order to recognize also credentials issued by third parties, simplifying the process of logging in.

The configuration page will read a Credential definition and let the administrator decide which attributes will be provided by the user during registration and which ones will be filled in by the administrator and will become the claims in the credential. Users will have the possibility to be recognized as returning users without providing any personal data. WordPress roles are supported and it will be possible to map WordPress roles to specific claims of the credentials. The community of developers will also be able to extend the basic functionality to develop e.g. observer applications able to use the login events, bridges to other mechanisms of authentication, etc.

The component is a standalone WordPress plugin, so it can be hosted on any WordPress server, and is written in PHP language. Our aim is to connect to SSI/eSSIF-Aries-compatible agents using REST APIs or ACA-Py remote private setups in order to perform all of its functions: for example issuing, verifying and revoking of credentials. The plugin can be configured using the WordPress admin role for flexibility, including: connection data to the eSSIF agents, verification rules and role mappings with claims. The configuration is saved to the WordPress database.

We also plan to modify the WordPress "user meta" persistence system in order to have all sensitive user data crypted on the database server; we'll give the option to webmasters to requests, via a claim at login, a (disposable) key which is held only by the user in its wallet to decrypt server-side user data during the user session only (data-at-rest encryption). Finally, to give WordPress an additional degree of freedom, we are also proposing to implement a PHP controller for the http admin interface of any ACA-Py setup hosted on private remote servers (not on the same webhosting).

## Architecture

![WordPreSSI Architecture](/assets/diagrams/wordpressi_architecture.png)

*WordPreSSI Architecture*

## About our team

Associazione Blockchain Italia is an Italian non-profit association for scientific research and multidisciplinary dissemination about blockchain technologies.

We have a solid team of members with strong expertise in the SSI area and a multi-disciplinary background (technical, legal, compliance, and more).

We have been leading our working table of Experts on SSI technologies for about two years. We have already developed two complete projects based on SSI technologies (Sovrin, Streetcred/Trinsic, ...) as final theses for the Computer Science Engineering course at University of Rome. We already created a fully working PoC using Trinsic APIs which can be found in the GitHub repository and we have already developed an OAuth plugin for WordPress.
